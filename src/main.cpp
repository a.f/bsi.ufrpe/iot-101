#include <Arduino.h>
#include <SPI.h>
#include "SSD1306.h"
#include <OneWire.h>
#include <DallasTemperature.h>
#include <TimeAlarms.h>
#include <WiFi.h>
#include <PubSubClient.h>

#include <vars.env>

// LORA SETTINGS
// #define SCK LORA_SCK   // GPIO5  -- SX1278's SCK
// #define MISO LORA_MISO // GPIO19 -- SX1278's MISO
// #define MOSI LORA_MOSI // GPIO27 -- SX1278's MOSI
// #define SS LORA_CS     // GPIO18 -- SX1278's CS
// #define RST LORA_RST   // GPIO14 -- SX1278's RESET
// #define DI0 LORA_IRQ   // GPIO26 -- SX1278's IRQ(Interrupt Request)
// #define BAND 915E6

// THINGSBOARD SETTINGS
#define TB_SRV "192.168.2.252"
#define MQTT_PORT 1883

// DATE AND TIME SETTINGS
#define NTP_SRV "a.st1.ntp.br"
#define UPDATE_INTERVAL 600E3
#define NTP_TZ "GMT+3"
#define NTP_DST 0

// WIRELESS CONNECTIVITY
#ifndef STA_SSID
#define STASSID "your-ssid"
#endif

#ifndef STA_PSK
#define STAPSK "your-password"
#endif

// APP SETTINGS
#define IPIN KEY_BUILTIN
#define OW_BUS 25
#define MAX_READINGS 60
#define LAST_READINGS_SIZE 10
#define READINGS_INTERVAL 5E2
#define ALARM_DELAY 1E3
#define AL_READ_TEMP 10
#define BUFSIZE 30

// INITIALIZATIONS
// ONEWIRE + DALLAS TEMP SENSOR
OneWire oneWireBus(OW_BUS);
DallasTemperature sensors(&oneWireBus);

// OLED DISPLAY
SSD1306 display(0x3c, OLED_SDA, OLED_SCL);

// WIFI
WiFiClient wifiClient;

// MQTT
PubSubClient mqttClient(wifiClient);

// MISC
float inTemp;
float outTemp;
uint32_t idx;
uint32_t counter = -1;
char readings[MAX_READINGS][BUFSIZE] = {{""}};

// FUNCTIONS
void blink(uint8_t ledPin = LED_BUILTIN, uint8_t times = 1, uint16_t delayOn = 1000, uint16_t delayOff = 1000)
{
    digitalWrite(ledPin, LOW);
    for (uint16_t i = 0; i < times; i++)
    {
        digitalWrite(ledPin, HIGH);
        delay(delayOn);
        digitalWrite(ledPin, LOW);
        delay(delayOff);
    };
}

void connectWifi(char ssid[], char psk[])
{
    Serial.print("Connecting to Wi-Fi...");
    WiFi.begin(ssid, psk);

    while (WiFi.status() != WL_CONNECTED)
    {
        delay(500);
        Serial.print(".");
    }
    Serial.println();
    Serial.print("Connected through IP ");
    Serial.println(WiFi.localIP().toString());
}

void connectMqtt(const char token[], uint16_t delayMs = 2000)
{
    Serial.print("Connecting to ThingsBoard (MQTT)...  ");

    if (mqttClient.connect("ESP32", token, NULL))
    {
        Serial.println("Connected!");
    }
    else
    {
        Serial.printf("Failed! [rc=%s]\n", mqttClient.state());
        Serial.printf("Retrying to connect in %.1f seconds...\n", delayMs / 1000.0);
        delay(delayMs);
    }
}

void initDisplay(uint8_t rstPin = OLED_RST)
{
    Serial.println("Setting up display...");
    digitalWrite(rstPin, LOW); // set GPIO16 low to reset OLED
    delay(50);
    digitalWrite(rstPin, HIGH); // while OLED is running, must set GPIO16 in high

    Serial.println("Initializing display...");
    display.init();
    display.flipScreenVertically();
    display.setFont(ArialMT_Plain_10);
    display.setTextAlignment(TEXT_ALIGN_LEFT);
}

void setupDoneInfo(uint16_t delayMs = 1500)
{
    char buffer[40];
    display.clear();
    display.drawString(0, 0, "Date and Time from NTP");
    display.drawStringf(0, 10, buffer, "Date: %02d-%02d-%04d", day(), month(), year());
    display.drawStringf(0, 20, buffer, "Time: %02d:%02d:%02d", hour(), minute(), second());
    display.drawStringf(0, 30, buffer, "IP: %s", WiFi.localIP().toString().c_str());
    display.drawStringf(0, 40, buffer, "### SETUP DONE ###");
    display.display();
    Serial.println("\n### Setup done ###\n\n");
    delay(delayMs);
}

void printLocalTime()
{
    struct tm timeinfo;
    if (!getLocalTime(&timeinfo))
    {
        Serial.println("Failed to obtain time");
        return;
    }
    Serial.println(&timeinfo, "%A, %B %d %Y %H:%M:%S");
}

void setDateTime(char tz[], uint16_t dst, char server[], int tries = 3)
{
    Serial.println("Setting time...");
    configTzTime(tz, server);

    struct tm timeinfo;
    for (int i = 0; i < tries; i++)
    {
        if (getLocalTime(&timeinfo))
        {
            break;
        }
        Serial.printf("[%d/%d] attempts to obtain time failed.\n", i + 1, tries);
    }
    setTime(timeinfo.tm_hour,
            timeinfo.tm_min,
            timeinfo.tm_sec,
            timeinfo.tm_mday,
            timeinfo.tm_mon + 1,
            timeinfo.tm_year + 1900); // set time by NTP server

    Serial.println(&timeinfo, "NTP Date and Time: %A, %B %d %Y %H:%M:%S");
}

void showLastReadings()
{
    uint8_t currIndex;
    uint32_t lastReadingsSize = LAST_READINGS_SIZE < counter ? LAST_READINGS_SIZE : counter + 1;
    uint32_t lastCounter = counter;

    Serial.printf("\n\nLast %d recorded values.\n", lastReadingsSize);

    for (uint8_t i = 0; i < lastReadingsSize; i++)
    {
        currIndex = (lastCounter - i) % 60;
        Serial.printf("[%02d] ", lastReadingsSize - i);
        Serial.println(readings[currIndex]);
    };
    Serial.println("");
}

void readTemp()
{
    blink(LED_BUILTIN, 2, 200, 0);
    sensors.requestTemperatures();
    char buf[BUFSIZE];

    noInterrupts();
    counter++;
    idx = counter % 60;
    inTemp = sensors.getTempCByIndex(0);
    outTemp = inTemp - random(100, 200) / 100.0;
    snprintf(buf, BUFSIZE, "%02d-%02d-%04d %02d:%02d:%02d > %.02fºC",
             day(),
             month(),
             year(),
             hour(),
             minute(),
             second(),
             inTemp);
    strncpy(readings[idx], buf, sizeof(buf));
    interrupts();
}

void digitalClockDisplay()
{
    // Serial.printf("%02d-%02d-%04d %02d:%02d:%02d #%d Temp: %.2fºC\n",
    //               day(),
    //               month(),
    //               year(),
    //               hour(),
    //               minute(),
    //               second(),
    //               counter + 1,
    //               currTemp);

    char buffer[40];

    display.clear();
    display.drawStringf(0, 0, buffer, "IP: %s", WiFi.localIP().toString().c_str());
    display.drawStringf(0, 10, buffer, "Date: %02d-%02d-%04d", day(), month(), year());
    display.drawStringf(0, 20, buffer, "Time: %02d:%02d:%02d", hour(), minute(), second());
    display.drawStringf(0, 30, buffer, "Temp: %.2f ºC", inTemp);
    display.drawStringf(0, 40, buffer, "#%06d", counter + 1);
    display.display();
}

void sendTemperatureData(const char token[], bool outside = false)
{
    connectMqtt(token, 2000);
    Serial.println("# Sending data to ThingsBoard #");

    char payload[40];
    float temp = 0;

    if (outside)
    {
        temp = outTemp;
    }
    else
    {
        temp = inTemp;
    }
    snprintf(payload, BUFSIZE, "{\"temperature\": %0.2f}", temp);
    Serial.printf("Payload: %s\n\n", payload);
    mqttClient.publish(topic, payload);
    mqttClient.disconnect();
}

void setup()
{
    Serial.begin(115200);
    while (!Serial)
        ;

    Serial.println("Setting up pins...");
    pinMode(16, OUTPUT);
    pinMode(LED_BUILTIN, OUTPUT);
    pinMode(IPIN, INPUT_PULLUP);

    Serial.println("Setting up the device...");
    blink(LED_BUILTIN, 5, 250, 100);

    connectWifi(STA_SSID, STA_PSK);

    setDateTime(NTP_TZ, NTP_DST, NTP_SRV, 2);

    Serial.println("Setting MQTT server...");
    mqttClient.setServer(TB_SRV, MQTT_PORT);
    mqttClient.setKeepAlive(60);

    Serial.println("Setting alarms...");
    Alarm.timerRepeat(AL_READ_TEMP, readTemp);

    Serial.println("Setting interruption...");
    attachInterrupt(
        digitalPinToInterrupt(IPIN),
        showLastReadings,
        RISING);
    /* LOW aciona a ISR quando o nível do pino é baixo
       CHANGE aciona a ISR quando o valor do pino muda
       RISING aciona a ISR quando o nível do pino vai de low para high
       FALLING aciona a ISR quando o nível do pino vai de high para low */

    Serial.println("Initializing sensors...");
    sensors.begin();

    initDisplay(OLED_RST);

    readTemp();

    setupDoneInfo(2500);
}

void loop()
{
    Serial.println("---");
    sendTemperatureData(token1);
    sendTemperatureData(token2, true);
    Alarm.delay(ALARM_DELAY);
    digitalClockDisplay();
}